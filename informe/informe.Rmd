---
title : Aplicación de ACM a datos socioeconomícos
subtitle : Análisis Multivariado 1
author : Mauro Loprete y Fabricio Machado
date: "`r paste('Compilado por última vez',Sys.time())`"
link: Markdown `r shiny::icon("markdown")`
output :
    rmdformats::robobook:
        code_folding: hide
        self_contained: true
        lightbox: true
        use_bookdown: true
        fig_caption: true
        includes:
            after_body: src/footer.html
    pdf_document: default
bibliography: [bib/packages.bib,bib/references.bib]
nocite: '@*'
biblio-style: apacite
link-citations: true
css: src/style.css
editor_options: 
  markdown: 
    wrap: 72
---

```{r setup, include=FALSE}
if(!require(pacman)) install.packages("pacman")
pacman::p_load(
    tidytable,
    purrr,
    glue,
    gt,
    here,
    magrittr,
    patchwork,
    ggplot2,
    fontawesome,
    ggridges,
    FactoMineR,
    factoextra,
    fastDummies
)

knitr::opts_chunk$set(warning = FALSE, message = FALSE,echo = FALSE)


source(
    here::here(
        "R",
        "carga_datos.R"
    )
)

theme_set(
  theme_ridges()
)


```

```{asis, echo=!knitr::is_latex_output()}
::: {.infobox}
**Resumen**

El objetivo del presente trabajo es realizar una aplicación de las técnicas del análisis factorial en concreto el de Análisis de Correspondencia Múltiple, 
para entender las relaciones entre variables de una base con variables de ahorro, nivel educativo y diferentes variables de posesión de confort. Fundamentando los pasos seguidos y las decisiones tomadas ante eventuales desafíos encontrados, mostrando tablas interpteraciones y su implementación en R [@R-base].

:::
```

# Introducción

## Fuente de datos

Se cuenta con datos procesados de la Encuesta Continua de Hogares (ECH)
de la edición 2010. Las variables incluidas contienen información de los
hogares tales como, ahorro, educación del jefe, disponibilidad de
ciertos electrodomésticos, posesión de autos, residentes propietarios,
percepción de rentas, cantidad de electrodomésticos, y presencia de
necesidades básicas insatisfechas (NBI) [^1]. 

Algunas de estas variables son habitualmente consideradas en estudios a cerca de la pobreza
multidimensional. Este enfoque trata de entender el fenómeno de la
pobreza más allá de la medición del ingreso, e incorpora privaciones de
capacidades que limitan la posibilidad de desarrollar una vida plena. 

La base combina además variables que hacen foco en la parte alta de la
distribución del ingreso como rentas, y ahorro. Una posible derivación
del análisis es encontrar categorías que permitan agrupar hogares según
distintos estatus de riqueza.

[^1]: Los meta datos se encuentran presentes en el anexo A.

## Objetivo

Dada la cantidad de variables cualitativas en la base nos proponemos
realizar un Análisis de Correspondencia Múltiple (ACM). Identificamos
ademas que las distintas variables sobre la posesión de
electrodomésticos pueden solaparse con la variable que refiere a la
cantidad de los mismos en el hogar, por lo que debemos hacer un análisis
por separado, para así comprender si es posible descartar
las dummies y conservar solo la categórica de electrodomésticos para el
análisis final. 

Dado que AHORROA y AHORROB son distintas estimaciones de
la deuda de los hogares, y por construcción la primera contiene datos
como facturas no pagadas al momento de la entrevista, que la segunda no,
utilizaremos únicamente AHORROA. 

## Análsis descriptivo

### Variable de Ahorro

La variable AHORROA equivale a la diferencia entre el ingreso y el ahorro, sobre el ingreso, por lo que hogares con ingresos bajos pueden tener ahorros más altos que hogares con ingresos bajos, aunque la magnitud del ahorro de los últimos sea mayor. A continuación se presenta su distribución. 

```{r,fig.cap = 'Distribución de la variable de ahorro'}
plt1 <- datos %>% 
 ggplot(
     aes(
         y = AHORROA
     )
 )  +
 stat_boxplot(
     geom = "errorbar", 
     width = 0.4
 ) + 
 geom_boxplot() + 
 coord_flip() + 
 theme(
     axis.text.y = element_blank(), axis.ticks.y = element_blank()
 ) + 
 scale_y_continuous(
     breaks = c(-2, -1.5, -1, -0.5, 0, 0.5, 1), 
     limits = c(-2, 1)
 )

plt2 <- datos %>% 
  ggplot(aes(x = AHORROA)) + 
  geom_histogram(
      aes(y = ..density..), 
      binwidth = 0.1, 
      colour = 1, 
      fill = "white"
  ) + 
  geom_density(
      lwd = 1, 
      colour = 4, 
      fill = 4, 
      alpha = 0.25
  ) + 
  labs(
      y  = "Densidad",
      x = ""
  ) + 
  scale_x_continuous(
      breaks = c(-2, -1.5, -1, -0.5, 0, 0.5, 1), 
      limits = c(-2, 1)
  )
  

plt2 + plt1 + plot_layout(nrow = 2, heights = c(2, 1)) + labs(caption = "Elaboración propia con base en datos riqueza ECH 2010")

```

```{r,fig.align="center", fig.cap = 'Hola'}

source(
    here(
        'R',
        'tabla.R'
    )
)
```

```{r, echo = T}
ri <- quantile(datos$AHORROA, probs = 0.75) - quantile(datos$AHORROA, probs = 0.25)

low <- quantile(datos$AHORROA, probs = 0.25) - 1.5*ri

upp <- quantile(datos$AHORROA, probs = 0.75) + 1.5*ri

dist <- (upp - low )/ 3

cut1 <- low + dist

cut2 <- upp - dist

datos %<>% 
mutate.(
    CAPAHORRO = ifelse.(AHORROA > 0, "CAP", "NOCAP")
) %>% 
mutate.(
  across.(
    .cols = - AHORROA,
    .fns = ~ factor(.x)
  )
)
```

Antes de ser exhaustivos con la metodología referida al ACM realizaremos
un análisis descriptivo de las variables disponibles, para esto se
realizó una tabla de frecuencias para cada variable presentada a continuación.

### Análisis por categoría

```{r}
datos1 <- datos %>% 
  select.(
    -AHORROA,
    -CAPAHORRO
  )
  
map_dfr(
    .x = names(datos1),
    .f = function(x) {

        table_prop(
            .data = datos1,
            .var = glue::glue(x)
        )       

    }
) %>% 
arrange.(
    var,
    res
) %>%
gt(
    rowname_col = 'var',
    caption = 'Análisis descriptivo inicial'
) %>%
tab_header(
    title = md("**Tabla de frecuencias por categorias**")
) %>% 
cols_label(
    var = md('**Variable**'),
    res = md('**Resultado**'),
    prop = md('**Porcentaje**')
) %>% 
fmt_percent(
    columns = 'prop'
) %>% 
tab_row_group(
    label = md('**Educación del jefe del hogar**'),
    rows = 'EDUCFIN'
) %>% 
tab_row_group(
    label = md('**Propietarios de casa donde residen**'),
    rows = 'CASA'
) %>% 
tab_row_group(
    label = md('**Posesión de auto**'),
    rows = "AUTO"
) %>% 
tab_row_group(
    label = md('**Recibe renta de alquiler de otras propiedades**'),
    rows = 'ALQUILER'
) %>% 
tab_row_group(
    label = md('**Posesión de Microondas**'),
    rows = 'MICROON'
) %>% 
tab_row_group(
    label = md('**Posesión de Multiprocesadora**'),
    rows = 'MULTIP'
) %>% 
tab_row_group(
    label = md('**Posesión de Lavarropas**'),
    rows = 'LAVARR'
) %>% 
tab_row_group(
    label = md('**Posesión de Calefón**'),
    rows = 'CALEFON'
) %>% 
tab_row_group(
    label = md('**Posesión Aspiradora**'),
    rows = 'ASPIRAD'
) %>% 
tab_row_group(
    label = md('**Posesión de Video**'),
    rows = 'VIDEO'
) %>% 
tab_row_group(
    label = md('**Posesión de electrodomesticos**'),
    rows = 'ELECUALI'
) %>% 
tab_row_group(
    label = md('**El hogar tiene al menos una NBI**'),
    rows = 'NBI'
) %>% 
tab_options(
    table.width = pct(100)
) %>% 
opt_align_table_header('left')

```

Las variables NBI y ALQUILER a priori hacen foco en grupos opuestos en términos de riqueza, dado que una es un indicador de pobreza y la otra hace referencia a la percepción de rentas. Ambos grupos son minoritarios dentro de su respectiva clasificación, un 6,73% de los hogares poseen NBI, y un 7,99% perciben rentas. 

No poseer NBI o no percibir rentas no implica ser rico o pobre respectivamente, por lo que se tienen grupos heterogéneos. Nos parece útil observar el comportamiento del ahorro dentro de las categorías de cada estas dos variables.

```{r,fig.cap = "Distribución del ahorro por si el hogar percibe alquiler"}
Base  <- read.table(
  here::here(
    'data',
    'riqueza312.txt'
  ),
  header = TRUE
) %>% 
select.(
  -c(
    RowNames,
    AHORROB
  )
)

Base %>%
    mutate.(
      ALQUILER = case_when.(
        ALQUILER == 1 ~ "Si",
        TRUE ~ "No"
      )
    ) %>%
    ggplot(
        aes(
            x = AHORROA,
            y = ALQUILER,
            fill = ALQUILER
        )
    ) + 
    geom_density_ridges() +
    theme_ridges() +
    labs(
      y = "",
      title = "Recibe renta de alquiler de otras propiedades",
      caption = "Elaboración propia con base en datos riqueza ECH 2010"
    ) +
    theme(
      legend.position = "none"
    )
```

```{r,fig.cap = "Distribución del ahorro por si el hogar tiene NBI"}
Base %>%
  mutate.(
      NBI = case_when.(
        NBI == 1 ~ "Si",
        TRUE ~ "No"
      )
    ) %>%
    ggplot(
        aes(
            x = AHORROA,
            y = NBI,
            fill = NBI
        )
    ) + 
    geom_density_ridges() +
    theme_ridges() + 
    labs(
      y = "",
      title = "El hogar tiene al menos una NBI",
      caption = "Elaboración propia con base en datos riqueza ECH 2010"
    ) + 
    theme(
      legend.position = "none"
    )
```

Vemos que para los hogares que tienen al menos una NBI la distribución del ahorro se vuelve bimodal acumulando observaciones en valores más bajos, y con una mayor proporción de datos atípicos negativos. Por otro lado los hogares que perciben rentas, comparados con los que no las perciben, acumulan una mayor parte de su distribución en valores más altos de ahorro. Tanto los hogares que no tienen NBI como los que no perciben rentas poseen una distribución más cercana a la del ahorro para todas las observaciones.

# Metodología

## ACM como ténica factorial

El Análisis de Correspondencia Múltiple (ACM) se encuentra contenido
dentro de los métodos del Análisis Factorial, su objetivo principal es
resumir variables en un número reducido de dimensiones o factores
perdiendo la menor cantidad de información posible, donde contamos con
variables del tipo cualitativas.

El ACM aplica para variables categóricas a diferencia del Análisis de
Componentes Principales (ACP) que trabaja con variables cuantitativas.


Otro análisis cualitativo dentro de las técnicas del análisis factorial
es el Análisis de Correspondencia Simple (ACS) que permite estudiar
variables categóricas pero solo relaciones dos a dos, debido a que su
insumo son las tablas de contingencia, limitando algunos tipos de
análisis.

## Diferentes enfoques

El ACM nos permite trabajar con múltiples variables cualitativas
previamente transformando nuestra matriz de datos original a un arreglo
de individuos variables, donde en cada uno de sus elementos tendremos
valores cero o uno, denominada **Tablas Disjuntas**. Dentro del mismo
marco de análisis podemos disponer de nuestra matriz de datos en forma de
un tipo de tabla particular, conocida como **Tablas de Burt**, que
surjan a partir de las tablas desjuntas y que proviene de una extensión
natural de las tablas de contingencia.

Una vez elegido el enfoque, aplicaremos ACS a las tablas disjuntas o ACS a los componentes diagonales de la tabla de Burt.

## Correciones sobre la inercia

Ambas perspectivas son casi equivalentes, generan las mismas coordenadas estándar para cada categoría,
la diferencia de cada una de ellas radica en los valores de las inercias, en el caso de la **tabla de burt** 
son los cuadrados de la **tabla disjunta**, por lo tanto estos últimos siempre serán mas pesimistas que los anteriores.

Sin embargo, el aplicar el ACM la inercia se infla artificialmente y, por lo tanto el porcentaje de la inercia es incorrecto.

Para corregir esto, se proponen dos alternativas, la primera se debe a Benzécri 1979 y la segunda a M. J. Greenacre 1993.
Ambas correcciones tienen en cuenta que valores propios menores a $1/J$ están codificando las dimensiones adicionales.

Sea $\lambda_{l}$ el valor propio $l-\text{ésimo}$ obtenido a partir de aplicar ACS a una tabla disjunta y $J$ la cantidad de variables.


### Correción de Benzécri

\begin{equation*}

\lambda_{l}^{*} = \left[\left(\frac{J}{J-1}\right) \times \left(\lambda_{l}-\frac{1}{J}\right)\right]^{2} I_{\lambda_{j} > \frac{1}{J}}

\end{equation*}

[@benzecri]


Una vez hecha esta corrección, se calcula el porcentaje de inercia como la suma de los valores propios, sin embargo esta corrección dará una imagen optimista [@Abdi], por lo que presentaremos la siguiente alternativa.
 
### Correción de Greenacre

Esta corrección propone evaluar el porcentaje de inercia relativo a la inercia fuera de los bloques de la diagonal de la tabla de Burt quitando el ruido de proveniente de bloques diagonales

El porcentaje de la inercia puede calcularse se la siguiente manera :


\begin{equation*}

\phi = \frac{J}{J-1} \times \left(\sum_{l}{\lambda_{l}^{2}} - \frac{J-K}{K^{2}}\right)

\end{equation*}

Una vez hecho esto, el porcentaje de inercia puede obtenerse mediante el ratio : 

\begin{equation*}

\psi_{c} = \frac{\lambda^{*}_{l}}{\phi}

\end{equation*}

[@greenacre_correspondence_1993]

A continuación, dejamos disponible el código para ajustar los valores propios en base a las metodologías anteriormente mencionadas.



```{r,echo = TRUE}
adj_benzeri <- function(mca,J) {
  h <- 1/J
  
  benzeri_aux <- function(eig) {
    aux = ((J/(J-1)) * (eig - h)) ^ 2
    
    eig_adj <- aux * as.numeric(eig > h)
    
  }
  
  map_dbl(
    .x = mca$eig[,1],
    .f = benzeri_aux
  )
  
  
}

adj_greenacre <- function(mca) {
  
  K <- nrow(mca$call$Xtot)
  J <- nrow(mca$call$X)
  
  eig <- mca$eig[,1]
    
    c <- (J/(J-1))
    aux <- sum((eig)^2) - (J-K)/(K^(2)) 
    psi <- c * aux
    
  eig_adj <- adj_benzeri(mca, J = J)
  
  prop_eig_adj <- eig_adj / psi
  
  tidytable(
    eig = eig,
    eig_adj = eig_adj,
    prop_eig = mca$eig[,2]/100,
    prop_eig_adj = prop_eig_adj,
    cum_eig = mca$eig[,3]/100,
    cum_eig_adj = cumsum(prop_eig_adj)
  )
    
}
```



# Resultados

## Primera aproximación

Aplicaremos ACM a todas las dummies de posesión de electrodomésticos y a 
la variable ELECUALI, antecedentes como Álvarez-Vaz y Castrillejo 2015
muestran una relación parabólica sobre el plano de los dos primeros
componentes de las distintas categorías de las variable dummies, que
permiten determinar una escala cualitativa.

```{r,echo = TRUE}
datos %>%
  select.(
    - c(
      AHORROA,
      EDUCFIN,
      CASA,
      AUTO,
      ALQUILER,
      NBI,
      CAPAHORRO
    )
  ) %>%
  mutate.(
    across.(
      .fns = ~ as.factor(.x)
    )
  ) %>%
  MCA(
    graph = FALSE
  ) %>%
  assign(
    "mca_electro",
    .,
    envir = .GlobalEnv
  )
```


```{r,fig.cap = "Gráfico del tipo biplot"}



fviz_mca_biplot(
  mca_electro,
  repel = TRUE
) + 
geom_function(
  fun = function(x) {
    x^2 - 1
  },
  colour = "black",
  linetype = 2,
  size = 1.2
) +
theme_ridges() +
labs(
  title = expression(paste("Biplot y función parábolica ",f(x) == x^{2}-1))
)
  
```


Nuestras variables responden de la misma manera y la variable ELECUALI
se ajusta a este comportamiento, estando asociadas la categoría mas baja
de posesión de electrodomésticos a no tener calefacción, y la categoría mas
alta a la posesión de microondas y multiprocesadora, ambas asociaciones
tienen bastante intensidad dada su lejanía del origen. Las demás
categorías de variables dummies están mas cerca del origen y ninguna
llega a tener una asociación lo suficientemente fuerte con la categoría
dos de ELECUALI, que parece tener mas poder explicativo. El primer
componente es quien explica la posesión de electrodomésticos como
lavarropa, aspiradora o vídeo.

Concluimos así que lo mejor es descartar las dummies y utilizar
únicamente ELECUALI para el análisis final. De aquí en más se mencionará de forma equivalente la cantidad de electrodomésticos y el comfort, dado que es habitual considerarlo así en la literatura sobre pobreza multidimensional.

## ACM por tramo de ahorro

Como mencionamos anteriormente, las variables de posesión de electrodomésticos puede resumirse
incluyendo la únicamente la variable ELECUALI, es por esto que aplicaremos esta técnica considerando esta variable y
el ahorro de los hogares caracterizado a tres niveles.

```{r,echo = TRUE}

datos %>%
    select.(
        - c(
            MICROON,
            MULTIP,
            LAVARR,
            CALEFON,
            ASPIRAD,
            VIDEO,
            CAPAHORRO,
            AHORROA
        )
    ) %>%
    MCA(
        X = .,
        graph = FALSE
    ) %>%
    assign(
        "acm_tramo",
        .,
        envir = .GlobalEnv
    )
```

A continuación presentaremos, una descomposición de la varianza ajustados por la corrección anteriormente mencionadas, los planos principales considerando el conjunto de variables, y una tabla indicando las coordenadas en los factores. 

```{r}

ajuste <- adj_greenacre(
  acm_tramo
)


```

```{r}
ajuste %>%
mutate.(
  dimension = 1:n(),
  .before = everything()
) %>%
gt(
    caption = 'Descomposición de la inercia tramos de Ahorro.'
) %>%
tab_header(
    title = md("**Tabla de la inercia ajustada**")
) %>%
cols_label(
    dimension = md("**Dimensión**"),
    eig = md('**Valor propio**'),
    eig_adj = md('**Valor propio ajustado**'),
    prop_eig = md("**Proporción de inercia total**"),
    prop_eig_adj = md('**Proporción de inercia total con correción**'),
    cum_eig = md("**Suma acumulada de la inercia total**"),
    cum_eig_adj = md('**Suma acumulada de la inercia total con correción**')
) %>% 
fmt_percent(
    columns = - c(eig_adj,eig,dimension)
) %>%
fmt_number(
  columns = c(eig_adj,eig),
  decimals = 4
) %>%
tab_options(
    table.width = pct(100)
) %>% 
opt_align_table_header('left')
  
```

Haciendo la corrección pertinente del aporte de los valores propios dada la inflación de la inercia, concluimos que con el primer plano (dimensiones uno y dos) se explica un 47,54% de la inercia global. 

La primer dimensión tiene un aporte del 34,65% a la inercia global, a partir de la segunda dimensión la explicación de la inercia por parte de estas pasa a ser similar, cada vez menor pero sin grandes saltos al comparar dos dimensiones consecutivas. No es muy distinto el 12,89% de inercia que explica la segunda dimensión al 10,86% de la tercera. 

```{r}
ajuste %>%
mutate.(
  dimension = 1:n(),
  .before = everything()
) %>% 
  ggplot(aes(x = dimension, y = cum_eig_adj)) +
  geom_line() +
  labs(x = "Dimensión", y = "Suma acumulada de la inercia corregida", caption = "Elaboración propia con base en datos riqueza ECH 2010") +
  scale_x_continuous(limits = c(1,8), breaks = c(1,2,3,4,5,6,7,8)) +
  scale_y_continuous(limits = c(0, 1), labels = scales::percent)
```

Al observar como se acumula la inercia en las dimensiones vemos como a partir de la tercera se forma una meseta, siendo menor el aporte de las dimensiones posteriores a esta.

Podemos tener más datos acerca de las dimensiones observando las coordenadas de las categorías en cada una.

```{r}


names_var <- row.names(acm_tramo$var$coord)

acm_tramo %$%
  var %$%
  coord %>%
  data.frame() %>%
  mutate.(
    names_var = names_var,
    .before = everything()
  ) %>%
  gt(
    caption = 'Tabla de Coordenadas tramos de ahorro'
  ) %>%  
  fmt_number(
    col = - names_var
  ) %>%
  cols_label(
    names_var = md("**Variable**"),
    Dim.1 = md("**Dim1**"),
    Dim.2 = md("**Dim2**"),
    Dim.3 = md("**Dim3**"),
    Dim.4 = md("**Dim4**"),
    Dim.5 = md("**Dim5**")
  ) %>%
  tab_options(
    table.width = pct(100)
  ) %>% 
  opt_align_table_header('left')


```

Vemos que la segunda dimensión tiene las categorías de educación ordenadas de forma ascendente en sus coordenadas, además de mayor distancia entre las dos categorías de NBI que las dimensiones que le siguen, cosa que se repite para las demás variables dicotómicas. La tercera dimensión presenta las categorías de ahorro por orden ascendente en sus coordenadas. 

Como a partir de la tercera dimensión las demás no hacen grandes aportes a la inercia, se tomará esta junto a la primera para conformar el plano. Se hablará de los distintos cuadrantes del plano fromado por las primeras dos dimensiones numerados de izquierda a derecha de arriba hacia abajo del 1 al 4.

```{r,fig.cap = "Plano principal de las variables AHORRO, ELECUALI y demás variables"}
fviz_mca_var(
  acm_tramo,
  axes = c(1,3),
  repel = TRUE
) + 
labs(
  title = "MCA: Análisis por categorías"
) +
theme_ridges()
```
 
La suma de 32,19% de la inercia global que aparece en los ejes refiere a la inercia sin ajustar, dato que se puiede ver en la tabla 3.1, la inercia explicada por el plano es de 47,54%. Las variables dummies al ser excluyentes se encuentran en cuadrantes opuestos evidenciando su relación negativa, lo que era esperable.

Vemos como el segundo componente diferencia entre grupos más ricos y menos ricos, tanto por niveles de educación, confort, NBI y percepción de rentas. Dentro del lado derecho del segundo componente se pueden diferenciar dos grupos de hogares más ricos divididos por el primer componente. En el cuadrante dos hay hogares con niveles altos de educación, alto comfort, posesión de auto y niveles altos de ahorro, que llamaremos profesionales. En el cuadrante cuatro los hogares tienen niveles medios de educación, los residentes son propietarios y reciben rentas, por lo que los llamaremos rentistas. La fuente de riqueza de estos dos grupos es distinta. Del lado izquierdo del plano también podemos diferenciar dos grupos con distintos determinantes de sus carencias. El cuadrante 1 tiene hogares con NBI, bajos niveles de comfort, no propietarios de sus hogares y con altos niveles de endeudamiento. El cuadrante tres tiene hogares con educación baja, sin auto y con niveles bajos tanto de ahorro como de endeudamiento, entendemos que están más asociados a bajos niveles de los retornos a la educación. 

Aquellos cuadrantes contrapuestos, parecen guardar relación con los motivos que hacen a los hogares más o menos pobres. Los cuadrantes 1 y 4 parecen diferenciarse en la posesión de propiedades, tanto por la casa donde se vive como por alquileres percibidos. Los cuadrantes 2 y 3 se diferencian por los distintos retornos a la educación, dado que contraponen valores extremos del nivel educativo alcanzado. 

Las variables EDUCFIN y AUTO poseen una asociación fuerte para sus dos categorías. 

```{r,fig.cap = "Contribución de las variables a la dimensión 1 y 2 en términos de cos2"}
fviz_cos2(
  acm_tramo,
  choice = "var",
  axes = 1:2
) + 
labs(
  title = "",
  y = "Contribución (%)",
  x = ""
) +
theme_ridges() + 
theme(
  axis.text.x = element_text(angle = 45, vjust = 0.5, hjust=1)
)
```

En cuanto a la contribución al plano (formado por los dos primeros componentes) de cada una de estas categorías vemos que las que más aportan son las categorías extremas de posesión de electrodomésticos, tener NBI, educación alta, posesión de auto, y que los residentes no sean propietarios. Como habíamos comentado al observar internamente las variables alquiler y NBI, las categorías de no posesión de estas dos condiciones generan grupos muy heterogéneos que no están focalizados ni en riqueza ni en pobreza, lo que se evidencia en su baja contribución a la inercia, algo similar ocurre con la educación intermedia. 

# Análisis de Cluster

```{r, eval =FALSE}
Clu <- acm_tramo %$%
  ind %$%
  coord %>%
  data.frame() %>%
  select.(
    Dim.1, 
    Dim.2, 
    Dim.3
  ) %>% 
  mutate.(
    nrow = seq.int(nrow(.)) 
  )

datos %<>% 
  mutate.(
    nrow = seq.int(nrow(.)) 
  )

Clu %<>% 
  left_join.(
    select.(datos, nrow, AHORROA),
    by = "nrow"
  ) %>% 
  select.(
    -nrow
  ) %>% 
  mutate.(
    AHORROA = as.numeric(AHORROA)
  )
```

```{r, eval=FALSE}
library(ggdendro) #Permite generar dendogramas con la misma logica que tidyverse
library(NbClust) #Libreria para identificar la cantidad de clusters optimo
library(GGally) #Para utilizar la funcion ggpairs del final. muy completa
library(cluster) #Para poder realizar cluster jerarquico

distancias <- get_dist(datos, method = "euclidean") #Calculo de la matriz de distancias

#fviz_dist(distancias, lab_size = 8) #Grafico de distancias

# CLuster jerarquico cualquiera de las 2 funciones hace lo mismo
CJ_ward <- agnes(distancias ,  method = "ward")
CJ_mascerc <- agnes(distancias, method = "single")
CJ_maslejos <- agnes(distancias, method = "complete")

indicadores(CJ_ward$merge, Clu) %>% 
  tail(20) %>% 
  gt(
    caption = "Reglas de detención"
  )

indicadores(CJ_ward$merge, Clu) %>% 
  tail(20) %>% 
  ggplot(aes(x = dimension, y = Rcuad)) +
  geom_line() +
  labs(x = "Dimensión", y = "Suma acumulada de la inercia corregida", caption = "Elaboración propia con base en datos riqueza ECH 2010") +
  scale_x_continuous(limits = c(1,8), breaks = c(1,2,3,4,5,6,7,8)) +
  scale_y_continuous(limits = c(0, 1), labels = scales::percent)

```


```{r, eval=FALSE}

#Comparamos con los de indicadores
source('indicadores.R')            
IND<-indicadores(CJ_ward$merge , datosst,10 )

#Comparamos con los de indicadores
source('indicadores.R')            
IND<-indicadores(CJ_mascerc$merge , datosst,10 )

#Descripcion de los grupos con ggplot

Clu$grupo<-cutree(CJ_ward , 3)
Clu$grupo<-as.factor(Clu$grupo)

#Graficos de caja para cada variable
ggplot(Clu, aes(x = grupo, y = Dim.1)) +   geom_boxplot(aes(fill = grupo))
ggplot(Clu, aes(x = grupo, y = Dim.2)) +   geom_boxplot(aes(fill = grupo))
ggplot(Clu, aes(x = grupo, y = Dim.3)) +   geom_boxplot(aes(fill = grupo))
ggplot(Clu, aes(x = grupo, y = AHORROA)) +   geom_boxplot(aes(fill = grupo))

#Graficos con varias variables emparejadas
ggplot(Clu, aes(x=Dim.1, y = Dim.2, color = grupo)) + geom_point()


#Grafico completo con ggpairs
ggpairs(Clu, 1:4, mapping = ggplot2::aes(color = grupo, alpha = 0.5), 
        diag = list(continuous = wrap("densityDiag")), 
        lower=list(continuous = wrap("points", alpha=0.9)))


#regiones y grupos
table(datos[,1],datos$grupo)
table(datos$grupo)

```


# Conculsiones

A partir de la aplicación de la técnica ACM pudimos obtener mayor información sobre las variables de nuestra base. Encontramos un gupo de variables que resultaron redundantes (electrodomésticos), y variables fuertemente asociadas como EDUCFIN y AUTO. Nos permitió encontrar grupos determinados por la presencia de distintas categorías, que implican distintos determinantes de la riqueza. 

El análisis auxiliar evidencia que la forma con la cual se codifica una variable cuantitativa para transformarla en una cualitativa puede cambiar nuestros resultados. Si bien no dimos con un criterio preestablecido estamos conformes con la capacidad interpretativa del implementado. 

Llegamos a niveles aceptables de explicación de la inercia global (47,54% en los dos primeros componentes), pudiendo aplicar la corrección acorde según la literatura. 

Encontramos cuatro grupos de estatus de riqueza, dos de ellos determinados inversamente por el nivel educativo (retornos a la educación) y los otros dos determinados inversamente por la posesión de propiedades.  Algunos de ellos con relaciones estrechas con otras variables dentro de la base como posesión de auto o calidad de los electrodomésticos. Las categorías con mayor peso en la distinción fueron la calidad de los electrodomésticos en general, la posesión de NBI, la educación baja y alta, posesión de auto y la percepción de rentas.

# Referencias

<div id="refs"></div>

```{r}
# knitr::write_bib(
#     c(
#         .packages(), 
#         'knitr',
#         'rmdformats'
#     ), 
#     here(
#         'bib',
#         "packages.bib"
#     )
# )
```


# Apendice {-} 

## A. Metadatos {-}

![](src/metadatos.png)

## B. Código {-}

